The program requires to be run from the .bat file in the main folder. An error message will present itself if a new game is started without an already present save file, but it will be created and you will be allowed to procede.

Division of Labour:

Mohammad Yasser Zaki:
-Supervision of work
-MVC architecture:
	-gameplay backend; data storage and display
	-game controls, such as exiting and pausing
-Debugging and organization
-Gameplay Adjustments
-Worked on Arcade mode
-Set up basis for the gameplay, including the factory and the game controller (see MVC)

Bassel Ashraf El-Sheikh:
-Created main-menu backend
-Created main-menu frontend
-Worked on link between main-menu and gameplay controller
-Created save/load system
-Debugging
-Save/Load system testing
-Provided music and artwork for fruits and bombs

Farah Ahmad Mohammad:
-Gameplay frontend (movement, fruit and bomb classes)
-Debugging
-Gameplay backend and frontend testing
-Gameplay Adjustments
-Worked on Arcade mode
-Implemented the special fruits

Mohammad Essameldeen:
-Created the documentation:
	-Created the UML and Sequence diagrams
-Worked on artwork for fruits and bombs